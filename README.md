# My Klipper config files

This repo is checked out as /home/pi/klipper-configs on my octopi. In `/home/pi/printer.cfg` one of the files in here is linked with this content:

```
[include klipper-configs/printer.ender3-nimble.cfg]
```
