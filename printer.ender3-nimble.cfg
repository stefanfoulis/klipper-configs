# This file contains common pin mappings for the 2018 Creality
# Ender 3. To use this config, the firmware should be compiled for the
# AVR atmega1284p.

# Note, a number of Melzi boards are shipped with a bootloader that
# requires the following command to flash the board:
#  avrdude -p atmega1284p -c arduino -b 57600 -P /dev/ttyUSB0 -U out/klipper.elf.hex
# If the above command does not work and "make flash" does not work
# then one may need to flash a bootloader to the board - see the
# Klipper docs/Bootloaders.md file for more information.

# See the example.cfg file for a description of available parameters.

[stepper_x]
step_pin: PD7
dir_pin: !PC5
enable_pin: !PD6
step_distance: .0125
endstop_pin: ^PC2
position_endstop: 0
position_max: 235
homing_speed: 50

[stepper_y]
step_pin: PC6
dir_pin: !PC7
enable_pin: !PD6
step_distance: .0125
endstop_pin: ^PC3
position_endstop: 0
position_max: 235
homing_speed: 50

[stepper_z]
step_pin: PB3
dir_pin: PB2
enable_pin: !PA5
step_distance: .0025
endstop_pin: probe:z_virtual_endstop
position_max: 250
position_min: -3

[extruder]
# Our creality 42-40 extruder stepper: https://www.reprap.me/creality-3d-two-phase-42-40-reprap-42mm-stepper-motor-for-ender-3-3d-printer.html
# - 1.8 degrees
# - Nominal Voltage: 4.83V
# - Current Rating: 1.5 (A)
# - Rated speed: 1-1000 (rpm)
# - Rated torque: 0.4 (NM)
# - CPU Atmel 8bit: should use 600 steps/mm at 1/4 microstepping -- whatever that means.
# https://zestytechnology.readthedocs.io/en/latest/nimble/tuning.html#configuring-microstepping
# klipper microsteps: https://github.com/KevinOConnor/klipper/blob/master/config/example-extras.cfg#L1276-L1334
# board has TMC2208 Stepper drivers
step_pin: PB1
dir_pin: !PB0
enable_pin: !PD6
# Original step_distance: 0.010526
# Zesty gear ratio is 30:1, so /30
# Correction after measuring: 0.0003508666667/50*56 = 0.0003929706667
# Round2 Correction: 0.0003929706667/50*49 = 0.0003851112534
step_distance: 0.0003851112534
max_extrude_only_velocity: 200
max_extrude_only_accel: 32
max_extrude_only_distance: 100.0
nozzle_diameter: 0.400
filament_diameter: 1.750
heater_pin: PD5
sensor_type: EPCOS 100K B57560G104F
sensor_pin: PA7
control: pid
pid_kp: 24.752
pid_ki: 1.387
pid_kd: 110.457
min_temp: 0
max_temp: 250
# Low value only for testing extruder without nozzle!!!
# min_extrude_temp: 15

[heater_bed]
heater_pin: PD4
sensor_type: EPCOS 100K B57560G104F
sensor_pin: PA6
control: pid
pid_kp: 64.598
pid_ki: 0.897
pid_kd: 1162.773
min_temp: 0
max_temp: 130

[fan]
pin: PB4

[mcu]
serial: /dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0

[printer]
kinematics: cartesian
max_velocity: 300
max_accel: 3000
max_z_velocity: 5
max_z_accel: 100

[display]
lcd_type: st7920
cs_pin: PA3
sclk_pin: PA1
sid_pin: PC1
encoder_pins: ^PD2, ^PD3
click_pin: ^!PC0

[bltouch]
sensor_pin: ^PC4
control_pin: PA4
pin_move_time: 0.4
# with custom bowden
#x_offset: -28
#y_offset: 0
#z_offset: 1.975
# with nimble custom
x_offset: 30.3
y_offset: 2.4
z_offset: 1.750

[bed_mesh]
speed: 90
horizontal_move_z: 5
min_point: 3, 40
max_point: 180, 205
probe_count: 3, 3
fade_start: 1.0
mesh_pps: 2, 2

[gcode_macro G29]
gcode:
    BED_MESH_CALIBRATE

[safe_z_home]
#home_xy_position: 235/2+28, 235/2
home_xy_position: 117, 117
speed: 80.0
z_hop: 10.0
z_hop_speed: 10.0
